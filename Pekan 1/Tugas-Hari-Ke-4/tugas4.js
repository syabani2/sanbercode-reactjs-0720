console.log('\n');
// Soal 1
console.log('LOOPING PERTAMA');
let i = 2
while(i <= 20) {
    console.log(i + " - I love Coding");
    i = i + 2;
}

console.log('\nLOOPING KEDUA');
while(i >= 2) {
    i = i - 2;
    if (i != 0) {
        console.log(i + " - I will become a frontend developer");
    }
}

console.log('\n');
// Soal 2
for(i = 1; i <= 20; i++) {
    if (i % 2 == 0) {
        console.log(i + " - Berkualitas")
    } else if (i % 3 == 0) {
        console.log(i + " - I Love Coding")
    } else {
        console.log(i + " - Santai")
    }
}

console.log('\n');
// Soal 3
for (i = 1; i <= 7; i++) {
    for (let j = i; j <= i; j++){
        let hashtag = '#'
        console.log(hashtag.repeat(j))
    }
}

console.log('\n');
// Soal 4
var kalimat="saya sangat senang belajar javascript"
var word = kalimat.split(" ");
console.log(word)

console.log('\n');
//Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah = daftarBuah.sort()

for(let i = 0; i < daftarBuah.length; i++) {
    console.log(daftarBuah[i]);
}