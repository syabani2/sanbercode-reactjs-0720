var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

readBooks(10000, books[0], function(remainingTime) {
    if (remainingTime) {
        readBooks(remainingTime, books[1], function(remainingTime) {
            if (remainingTime) {
                readBooks(remainingTime, books[2], function() {
                    console.log(`Saya sudah membaca semua buku`);
                })
            }
        })
    }
})
