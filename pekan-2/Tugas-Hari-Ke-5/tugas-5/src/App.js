import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="container">
        <h1>Form Pembelian Buah</h1>
        <table className="table">
          <tr>
            <th>
              Nama Pelanggan
            </th>
            <td>
              <input type="text" name="name" id="name" size="32"></input>
            </td>
          </tr>
          <tr>
            <th>
              Daftar Item
            </th>
            <td>
              <input type="checkbox" id="Semangka" name="Semangka" value="Semangka"></input>
              <label for="Semangka"> Semangka</label><br></br>
              <input type="checkbox" id="Jeruk" name="Jeruk" value="Jeruk"></input>
              <label for="Jeruk"> Jeruk</label><br></br>
              <input type="checkbox" id="Nanas" name="Nanas" value="Nanas"></input>
              <label for="Nanas"> Nanas</label><br></br>
              <input type="checkbox" id="Salak" name="Salak" value="Salak"></input>
              <label for="Salak"> Salak</label><br></br>
              <input type="checkbox" id="Anggur" name="Anggur" value="Anggur"></input>
              <label for="Anggur"> Anggur</label><br></br>
            </td>
          </tr>
          <tr>
            <td>
              <button type="submit">Kirim</button>
            </td>
          </tr>
        </table>
      </div>
    </div>
  );
}

export default App;
