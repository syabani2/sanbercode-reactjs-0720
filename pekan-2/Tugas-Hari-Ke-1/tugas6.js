// Soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var objDaftarPeserta = {
    "nama": arrayDaftarPeserta[0],
    "jenis kelamin": arrayDaftarPeserta[1],
    "hobi": arrayDaftarPeserta[2],
    "tahun lahir": arrayDaftarPeserta[3]
}

console.log(objDaftarPeserta);

// Soal2
let objDaftarBuah = [{
        "nama": "strawbery",
        "warna": "merah",
        "ada bijinya": "tidak",
        "harga": 9000
    },
    {
        "nama": "jeruk",
        "warna": "oranye",
        "ada bijinya": "ada",
        "harga": 8000
    },
    {
        "nama": "semangka",
        "warna": "hijau & merah",
        "ada bijinya": "ada",
        "harga": 10000
    },
    {
        "nama": "pisang",
        "warna": "kuning",
        "ada bijinya": "tidak",
        "harga": 5000
    },
]

console.log(objDaftarBuah[0]);

// Soal3
let dataFilm = [];

function addFilm(nama, durasi, genre, tahun) {
    let item = {
        nama: nama,
        durasi: durasi,
        genre: genre,
        tahun: tahun
    }

    dataFilm.push(item);
    return dataFilm;
}

console.log(addFilm('The Billionaire', 60, 'TV Dramas', 2015));

//Soal4
class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
        console.log('hop hop');
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    }

    yell() {
        console.log('Auoooo');
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

var sungokong = new Ape("kera sakti")
sungokong.yell()

var kodok = new Frog("buduk");
kodok.jump();

// Soal 5
class Clock {
    constructor(template) {
        this.template = template;
        this.timer;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        var t = this;
        this.timer = setInterval(function() {
            t.render();
        }, 1000);
    }

}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();