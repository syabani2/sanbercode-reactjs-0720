// Soal 1
let luasLingkaran = (diameter) => {
    return 1/4 * 3.14 * (diameter * diameter);
}

console.log(luasLingkaran(15));

let kelilingLingkaran = (jariJari) => {
    return 2 * 3.14 * jariJari;
}

console.log(kelilingLingkaran(65));

// Soal 2
let tambahKalimat = (kalimat, kata) => {
    kalimat = `${kalimat}${kata} `;
    return kalimat;
}

let kalimat = "";
let kata = ['saya', 'adalah', 'seorang', 'frontend', 'developer'];

for(let i = 0; i < kata.length; i++) {
    if (i < kata.length - 1) {
        kalimat = tambahKalimat(kalimat, kata[i]);
    } else {
        console.log(tambahKalimat(kalimat, "developer"));
    }
}

// Soal 3
class Book {
    constructor(name, totalPage, price) {
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price);
        this.isColorful = isColorful
    }
}

let book = new Book('Arah Langkah', 300, 75000);
let komik = new Komik('Haikyuu', 350, 20000, true);

console.log(book);
console.log(komik)